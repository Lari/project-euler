#!/usr/bin/pypy

import math

MAX = int(2e6)
root = math.sqrt(MAX)
numbers = list(range(2, MAX))
n = numbers[0]
i = 0
while n <= root:
    numbers = [x for x in numbers if x % n != 0 or x == n]
    i += 1
    n = numbers[i]
print(sum(numbers))
