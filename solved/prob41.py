#!/usr/bin/python

from euler import sieve
from math import sqrt
from itertools import permutations


primes = sieve(32000)
def is_prime(n):
    sqrtn = sqrt(n)
    if n < 2:
        return False
    if n % 2 == 0:
        return False
    if int(sqrtn)**2 == n:
        return False
    for i in primes:
        if n % i == 0:
            return False
        if i > sqrtn:
            break
    return True

largest = 0
numbers = "123456789"

while largest == 0:
    for i in permutations(numbers):
        candidate = int(''.join(i))
        if is_prime(candidate):
            if candidate > largest:
                largest = candidate
    if largest == 0:
        numbers = numbers[:-1]

print(largest)
