#!/usr/bin/python
#
# |       0x1, 1 reitti
#  _
# |_|     1x1, 2 reittiä
#  _ _
# |_|_|   2x1, 3 reittiä
#  _ _ _
# |_|_|_| 3x1, 4 reittiä
#       
# |
# |         0x2, 1 reitti
#  _
# |_|
# |_|       1x2, 3 reittiä
#  _ _
# |_|_|
# |_|_|     2x2, 6 reittiä
#  _ _ _
# |_|_|_|
# |_|_|_|   3x2, 10 reittiä
#  _ _ _ _
# |_|_|_|_|
# |_|_|_|_| 4x2, 15 reittiä
#
# 1, 1+1, 1+1+1, 1+1+1+1
# 1, 1+2, 1+2+3, 1+2+3+4
# 1, 1+(1+2), 1+(1+2)+(1+2+3), 1+(1+2)+(1+2+3)+(1+2+3+4)
# 1, 4, 10, 20
#
# 0x0: 0  = 0
# 1x1: 2  = 1 + 1
# 2x2: 6  = 1 + 2 + 3
# 3x3: 20 = 1 + (1+2) + (1+2+3) + (1+2+3+4)
# 4x4: 70 = 1 + (1 + 1+2) + (1 + 1+2 + 1+2+3) + (1 + 1+2 + 1+2+3 + 1+2+3+4) + (1 + 1+2 + 1+2+3 + 1+2+3+4 + 1+2+3+4+5)

#  2x2: 6  = 1 + 2 + 3
# summa = 0
# for i in range(1, 2 + 2):
#     summa += i
# print("2x2: 6 =", summa)
# 
#  3x3: 20 = 1 + (1 + 2) + (1 + 2 + 3) + (1 + 2 + 3 + 4)
# summa = 0
# for i in range(1, 3 + 2):
#     for j in range(1, i + 1):
#         summa += j
# print("3x3: 20 = ", summa)
# 
#  4x4: 70 = 1 + (1 + 1+2) + (1 + 1+2 + 1+2+3) + (1 + 1+2 + 1+2+3 + 1+2+3+4) + (1 + 1+2 + 1+2+3 + 1+2+3+4 + 1+2+3+4+5)
# summa = 0
# for i in range(1, 4 + 2):
#     for j in range(1, i + 1):
#         for k in range(1, j + 1):
#             summa += k
# print("4x4: 70 = ", summa)
#
# 5x5: 252
# summa = 0
# for i in range(1, 5 + 2):
#     for j in range(1, i + 1):
#         for k in range(1, j + 1):
#             for l in range(1, k + 1):
#                 summa += l
# print("5x5: 252 = ", summa)

#def inner_loop(i, depth, summa):
#    if depth > 1:
#        for j in range(1, i + 1):
#            summa = inner_loop(j, depth - 1, summa)
#    else:
#        summa += i * (i + 1) / 2
#    return summa
#
#def outer_loop(size):
#    summa = 0
#    for i in range(1, size + 2):
#        summa = inner_loop(i, size - 2, summa)
#    return summa
#
#print(outer_loop(20))

from math import factorial

def binomial(n, k):
    return factorial(n) / (factorial(n - k) * factorial(k))

def grid(n):
    return binomial(2 * n, n)

print(grid(20))
