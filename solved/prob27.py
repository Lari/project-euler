#!/usr/bin/python

from euler import sieve

primes = sieve(1000)
primes.insert(0, 1)

# 1. take two primes a and b
# 2. calculate n² + an + b, where n = 1 to 80
# 3. see how long there are primes
# 4. keep track of (a, b) pair that produces the longest prime-sequence

def make_list(a, b):
    results = []
    for n in range(80):
        results.append(n**2 + a*n + b)
    return results

def list_length(somelist):
    for i, v in enumerate(somelist):
        if v not in primes:
            break
    return i

longest = 0
longest_pair = (0, 0)

tests = ((1, 1), (1, -1), (-1, 1), (-1, -1))

for a in primes:
    for b in primes:
        for a_1, b_1 in tests:
            result = list_length(make_list(a_1 * a, b_1 * b))
            if result > longest:
                longest = result
                longest_pair = (a_1 * a, b_1 * b)

print(longest_pair[0] * longest_pair[1])

