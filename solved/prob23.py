#!/usr/bin/pypy

largest = 40000 #28123
abundants = set()
sum_of_two_abundants = set()

from euler import divisors

print('1/5 creating abundants up to ' + str(largest) + '.')
for i in range(1, largest):
    if sum(divisors(i, proper=True)) > i:
        abundants.add(i)

print('2/5 creating all numbers that are sums of two abundants up to ' + str(largest) + '.')
for i in abundants:
    for j in abundants:
        sum_of_two_abundants.add(j + i)

print("3/5 creating list from 1 to 28123.")
not_sum_of_two_abundants = set(range(1, largest + 1))
print("4/5 getting all numbers that are not sums of two abundants.")
not_sum_of_two_abundants -= sum_of_two_abundants
print("5/5 calculating sum of all numbers not sums of two abundants.")
#l = sorted(list(not_sum_of_two_abundants))
print(sum(not_sum_of_two_abundants))
#for i, v in enumerate(l):
#    print(i + 1, v)
