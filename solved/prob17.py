#!/usr/bin/python

ones = {1: "one", 2: "two", 3: "three", 4: "four",
        5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine"}

teens = {10: "ten", 11: "eleven", 12:"twelve", 13:"thirteen", 14: "fourteen", 15: "fifteen",
        16: "sixteen", 17:"seventeen", 18: "eighteen", 19:"nineteen"}

tens = {1: "ten", 2: "twenty", 3: "thirty", 4: "forty", 5: "fifty",
        6: "sixty", 7:"seventy", 8:"eighty", 9:"ninety"}
         
def name_of(n):
    letters = ""
    if len(n) == 4:
        return "onethousand"
    if len(n) == 3:
        letters += ones[ int(n[0]) ]
        letters += "hundred"
        n = n[1:3]
        n = str(int(n))
        if n == '0':
            return letters
    if len(n) == 2:
        if letters != "":
            letters += "and"
        if int(n[0]) > 1:
            letters += tens[int(n[0])]
            if n[1] != '0':
                letters += ones[int(n[1])]
        elif int(n[0]) == 1:
            letters += teens[int(n)]
        n = str(int(n))
        return letters
    if len(n) == 1:
        if letters != "":
            letters += "and"
        if n != '0':
            letters += ones[int(n)]
        return letters

one_thousand_names = ""
for i in range(1, 1001):
    one_thousand_names += name_of(str(i))
print(len(one_thousand_names))

