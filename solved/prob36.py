#!/usr/bin/python

from math import ceil, log

def base2(number):
    number_2 = []
    for i in range(ceil(log(number, 2))):
        number_2.append((number >> i) & 0x0001)
    return list(reversed(number_2))

total = 0
for i in range(1, 1000001, 2):
    if str(i) == ''.join(reversed(str(i))):
        binary = base2(i)
        if binary == list(reversed(binary)):
            total += i
print(total)
