#!/usr/bin/python

from euler import divisors

onetonine = "123456789"
pandigitals = []

for i in range(1000, 9999):
    divs = divisors(i, proper=True)
    del divs[0]
    for j in divs:
        stri = str(i) + str(int(j)) + str(int(i / j))
        if ''.join(sorted(stri)) == onetonine:
            pandigitals.append(i)
            break
print(sum(pandigitals))
