#!/usr/bin/pypy

from math import sqrt

# Hitauden syy oli neliöjuuren unohtaminen
def divisors(n):
    div = 2
    divisors = 1
    root = sqrt(n)
    while div < root:
        if n % div == 0:
            divisors += 1
        div += 1
    return divisors * 2

def main():
    sum = 3
    i = 2
    divisores = 1
    while divisores < 500:
        i += 1
        sum += i
        divisores = divisors(sum)
    print(sum)

if __name__=='__main__':
    main()
