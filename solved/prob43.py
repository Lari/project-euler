#!/usr/bin/pypy

from itertools import permutations

pandigit = "0123456789"
with_property = []

def test_property(d):
    if int(d[1:4]) % 2 == 0:
        if int(d[2:5]) % 3 == 0:
            if int(d[3:6]) % 5 == 0:
                if int(d[4:7]) % 7 == 0:
                    if int(d[5:8]) % 11 == 0:
                        if int(d[6:9]) % 13 == 0:
                            if int(d[7:10]) % 17 == 0:
                                with_property.append(d)

for i in permutations(pandigit):
    test_property(''.join(i))

print(sum(int(i) for i in with_property))
