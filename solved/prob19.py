#!/usr/bin/python

def months(year):
    if year % 4 == 0 and year != 2000:
        return (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    else:
        return (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

weekdays = ("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday")
days = -1
first_sunday_of_month = 0

for year in range(1900, 2001):
    for month in months(year):
        for day in range(month):
            days += 1
            if day == 0 and days % 7 == 0 and year > 1900:
                first_sunday_of_month += 1

print(first_sunday_of_month)
