#!/usr/bin/python

def is_palindrome(n):
    #b = bytearray(len(n))
    #for i in range(len(n)):
    #    b[i] = ord(n[len(n) - i - 1])
    if ''.join(list(reversed(list(str(n))))) == n:
        return True
    else:
        return False

largest = 0
for i in range(900, 1000):
    for j in range(900, 1000):
        product = i * j
        if is_palindrome(str(product)):
            if product > largest:
                largest = product
print(largest)
