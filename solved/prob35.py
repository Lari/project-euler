#!/usr/bin/pypy

from euler import sieve, timeit
from itertools import permutations

def circulate(some):
    n = len(some)
    circulations = [some]
    for i in range(len(some) - 1):
        circulations.append(circulations[-1][1:] + circulations[-1][0])
    return circulations

def main(n):
    print("Creating primes...")
    primes = set(sieve(n))
    circulars = set()
    print("Doing some preprocessing on primelist...")
    evens = set("02468")
    disqualified = set()
    for i in primes:
        if not evens.isdisjoint(set(str(i))):
            disqualified.add(i)
    primes -= disqualified
    primes.add(2)
    print("Starting main loop...")
    while len(primes) > 0:
        i = primes.pop()
        if i not in circulars:
            circ = set(int(''.join(x)) for x in set(circulate(str(i))))
            circ.remove(i)
            if circ <= primes:
                circulars |= circ | set([i])
                primes -= circ
    return circulars

if __name__ == '__main__':
    print(len(main(1000000)))
