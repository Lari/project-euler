#!/usr/bin/pypy

from euler import sieve
from copy import copy
from itertools import product, combinations
from time import time

d = 6
replaces = 3
numbers = "0123456789"
longest = 0

primes = sieve(10**d)
primes = set(x for x in primes if x > 10**(d - 1))

found = []

for s in product(list(numbers), repeat=(d-replaces)):
    for i in combinations(numbers[:d-1], replaces):
        temp = []
        for n in numbers:
            if n == "0":
                s2 = copy(list(s))
                for place in i: 
                    s2.insert(int(place), n)
            else:
                for place in i:
                    s2[int(place)] = n
            if int(s2[-1]) % 2 != 0:
                prob = ''.join(s2)
                if int(prob) in primes:
                    temp.append(prob)
        temple = len(temp)
        if temple:
            if temple > longest:
                longest = len(temp)
                found = []
            if temple == longest:
                found.append(temp)
print(found[0][0])
