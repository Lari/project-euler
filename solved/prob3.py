#!/usr/bin/python

import math

n = 600851475143
x = math.sqrt(n)
div = 2

while n > x or div < x:
    if n % div == 0:
        print(div)
        n /= div
    else:
        div += 1
print(n)
