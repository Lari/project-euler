#!/usr/bin/python

import math

def factors(n):
    "Returns prime factors of given number"
    factors = []
    if n < 2:
        raise ValueError('Too small number')
    div = 2
    square = math.sqrt(n)
    while n > square:
        if n % div == 0:
            n /= div
            factors.append(div)
        else:
            div += 1
    return factors

def divisors(n):
    "Returns all divisors of a given number"
    divisors = []
    square = math.sqrt(n)
    div = 2
    while div <= square:
        if n % div == 0:
            divisors.append(div)
        div += 1
    return divisors

if __name__ == '__main__':
    print(factors(2520))
    print(divisors(2520))
