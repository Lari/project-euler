#!/usr/bin/pypy

def sequence(n):
    terms = 1
    while n != 1:
        if n % 2 == 0:
            n /= 2
        else:
            n = 3*n + 1
        terms += 1
    return terms

longest_len = 0
longest = 0
for i in range(1, 1000000):
    s = sequence(i)
    if s > longest_len:
        longest = i
        longest_len = s
print(longest)


