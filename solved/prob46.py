#!/usr/bin/pypy

# What is the smallest odd composite that cannot be written as the sum of a
# prime and twice a square?

from euler import sieve

print("Generating primes...")
primes = sieve(1000000)

print("Generating composite numbers...")
composites = set(range(2, 1000000))
composites -= set(primes)

print("Filtering out even numbers...")
odd_composites = sorted([x for x in composites if x % 2 != 0])

twice_a_square = [2 * x**2 for x in range(1, 1000000)]

print("Searching...")
for number in odd_composites:
    found = False
    for i in twice_a_square:
        if i > number:
            break
        prob_prime = number - i
        if prob_prime in primes:
            #print(str(prob_prime) + " + " + str(i) + " = " + str(number))
            found = True
            break
    if not found:
        print(number)
        break
