#!/usr/bin/python

taulu = ((75,),
         (95, 64),
         (17, 47, 82),
         (18, 35, 87, 10),
         (20,  4, 82, 47, 65),
         (19,  1, 23, 75,  3, 34),
         (88,  2, 77, 73,  7, 63, 67),
         (99, 65,  4, 28,  6, 16, 70, 92),
         (41, 41, 26, 56, 83, 40, 80, 70, 33),
         (41, 48, 72, 33, 47, 32, 37, 16, 94, 29),
         (53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14),
         (70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57),
         (91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48),
         (63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31),
         ( 4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23),)

class memoized(object):
   """Decorator that caches a function's return value each time it is called.
   If called later with the same arguments, the cached value is returned, and
   not re-evaluated.
   """
   def __init__(self, func):
      self.func = func
      self.cache = {}
   def __call__(self, *args):
      try:
         return self.cache[args]
      except KeyError:
         value = self.func(*args)
         self.cache[args] = value
         return value
      except TypeError:
         # uncachable -- for instance, passing a list as an argument.
         # Better to not cache than to blow up entirely.
         return self.func(*args)
   def __repr__(self):
      """Return the function's docstring."""
      return self.func.__doc__
   def __get__(self, obj, objtype):
      """Support instance methods."""
      return functools.partial(self.__call__, obj)

def larger(x, y):
    if y == len(taulu) - 1:
        return taulu[y][x]
    else:
        n1 = larger(x, y + 1)
        n2 = larger(x + 1, y + 1)
        if n1 > n2:
            return (n1 + taulu[y][x])
        else:
            return (n2 + taulu[y][x])

if __name__ == '__main__':
    print(larger(0, 0))
