#!/usr/bin/python

from fractions import Fraction

results = {}

for i in range(1, 100):
    for j in range(i, 100):
        if len(str(i)) == len(str(j)):
            if Fraction(i, j) in results:
                results[Fraction(i, j)].append((i, j))
            else:
                results[Fraction(i, j)] = [(i, j)]

found = []

for i in results:
    if len(results[i]) > 1 and results[i][0][0] < 10 and results[i][0][1] < 10:
        filtered = sorted(results[i])
        ones = list((i for i in filtered if i[0] < 10))
        tens = list((i for i in filtered if i[0] >= 10))
        for i in ones:
            for j in tens:
                if (str(i[0]) in str(j[0]) and str(i[1]) in str(j[1]) and 
                    j[0] % 10 != 0 and j[0] % 11 != 0 and i[0] != i[1]):
                    first = list(map(int, str(j[0])))
                    second = [int(n) for n in str(j[1])]
                    del first[first.index(i[0])]
                    del second[second.index(i[1])]
                    if first[0] == second[0]:
                        found.append((i, j))
total = Fraction(1)
for i in found:
    total *= Fraction(*i[0])
    print(i[0][0], "/", i[0][1], "=", i[1][0], "/", i[1][1])

print("product of these is:", total)

