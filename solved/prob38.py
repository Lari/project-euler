#!/usr/bin/python

numbers = set("123456789")
pandigit = ""
largest = 0
for i in reversed(range(1000, 10000)):
    pandigit = str(i) + str(i * 2)
    if set(pandigit) == numbers:
        if largest < int(pandigit):
            largest = int(pandigit)

print(largest)

