#!/usr/bin/pypy

from math import factorial

total = 0
for number in range(3, 1000000):
    test = sum(factorial(int(i)) for i in str(number))
    if test == number:
        total += test
print(total)

