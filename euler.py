from math import sqrt, floor, ceil
from time import time

def divisors(n, proper=False):
    divisors = set([1])
    if n < 0:
        n = -n
    root = sqrt(n)
    for i in range(2, int(floor(root)) + 1):
        if n % i == 0:
            divisors.add(i)
    otherhalf = set()
    for i in divisors:
        otherhalf.add(n / i)
    if proper:
        otherhalf.remove(n)
    divisors.update(otherhalf)
    return [int(i) for i in divisors]

def sieve(n):
    """Takes in n and returns list containing all primes smaller than or
    equal to n. 
    
    Algorithm: Sieve of Eratosthenes
    
    """
    primelist = [] 
    numberlist = list(range(2, n + 1))
    while numberlist[0] < sqrt(n):
        primelist.append(numberlist[0])
        numberlist = list(filter(lambda x:x % primelist[-1], numberlist))
    return primelist + numberlist

def factorize(number):
    x = 2
    factors = []
    while number > 1:
        while number % x == 0:
            number /= x
            factors.append(x)
        x += 1
    return factors

def hamming(s1, s2):
	# taken from wikipedia
	assert len(s1) == len(s2)
	return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

# Decorators

class memoized(object):
   """Decorator that caches a function's return value each time it is called.
   If called later with the same arguments, the cached value is returned, and
   not re-evaluated.
   """
   def __init__(self, func):
      self.func = func
      self.cache = {}
   def __call__(self, *args):
      try:
         return self.cache[args]
      except KeyError:
         value = self.func(*args)
         self.cache[args] = value
         return value
      except TypeError:
         # uncachable -- for instance, passing a list as an argument.
         # Better to not cache than to blow up entirely.
         return self.func(*args)
   def __repr__(self):
      """Return the function's docstring."""
      return self.func.__doc__
   def __get__(self, obj, objtype):
      """Support instance methods."""
      return functools.partial(self.__call__, obj)

class timeit(object):
    """Time a function"""
    def __init__(self, f):
        self.f = f

    def __call__(self, *args):
        start = time()
        self.f(*args)
        print(self.f.__name__, "took", "{:.3f}".format(time() - start), "seconds to execute.")
