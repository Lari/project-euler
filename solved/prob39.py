#!/usr/bin/pypy

solutions = set()
longest = 1, 1

for i in range(3, 1001):
    solutions = set()
    for c in range(i // 3, i + 1):
        for b in range(1, i - c):
            a = i - b - c
            if a**2 + b**2 == c**2:
                solutions.add(tuple(sorted((a, b, c))))
    if len(solutions) >= longest[1]:
        longest = i, len(solutions)
        print("New!", longest)

print(longest)

