#!/usr/bin/pypy
#
# Problem 49
#
# The arithmetic sequence, 1487, 4817, 8147, in which each of the terms
# increases by 3330, is unusual in two ways: (i) each of the three terms are
# prime, and, (ii) each of the 4-digit numbers are permutations of one another.
#
# There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes,
# exhibiting this property, but there is one other 4-digit increasing sequence.
#
# What 12-digit number do you form by concatenating the three terms in this
# sequence?

from euler import sieve

def is_permutation(first, other):
    return sorted(str(first)) == sorted(str(other))

primes = [x for x in sieve(10000) if x > 1000]
found = 0
for width in range(2, 4999):
    for prime in primes:
        if prime + 2 * width < primes[-1]:
            second = prime + width
            third = prime + 2 * width
            if is_permutation(prime, second) and is_permutation(prime, third):
                if second in primes:
                    if third in primes:
                        found += 1
                        if found == 2:
                            break
    if found == 2:
        print(str(prime) + str(second) + str(third))
        break
