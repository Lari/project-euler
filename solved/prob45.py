#!/usr/bin/python

# Find the next triangle number that is also pentagonal and hexagonal

from math import sqrt

def triangle():
    n = 2
    while True:
        yield int(n * (n + 1) / 2)
        n += 1

def is_pentagonal(n):
    result = (sqrt(24 * n + 1) + 1) / 6
    return result % 1 == 0

def is_hexagonal(n):
    result = (sqrt(8 * n + 1) + 1) / 4
    return result % 1 == 0

for i in triangle():
    if is_pentagonal(i):
        if is_hexagonal(i):
            if i != 40755:
                print(i)
                break
