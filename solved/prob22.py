#!/usr/bin/python

from string import ascii_uppercase
from pprint import pprint

def namescore(name):
    total = 0
    for letter in name:
        total += ascii_uppercase.index(letter) + 1
    return total

def main():
    names = dict()
    with open("names.txt") as f:
        s = f.read()
        l = s.split(",")
        l = [i.strip('"') for i in l]
        for name in l:
            names[name] = namescore(name)
        for i, v in enumerate(sorted(names)):
            names[v] *= i + 1
        print(sum(names.values()))

if __name__=='__main__':
    main()
