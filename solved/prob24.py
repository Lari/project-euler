#!/usr/bin/python

from itertools import permutations

for i, v in enumerate(permutations("0123456789")):
    if i + 1 == 1000000:
        print(''.join(v))
        break
