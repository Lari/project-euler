#!/usr/bin/python

from euler import sieve

primes = set(sieve(1000000))
truncatables = []

def truncate(prime):
    prime = str(prime)
    truncations = []
    for i in range(1, len(prime)):
        truncations.append(prime[i:])
        truncations.append(prime[:i])
    return set(map(int, truncations))

for i in primes:
    if i > 10:
        if truncate(i) <= primes:
            truncatables.append(i)
        if len(truncatables) == 11:
            print("Found all!")
            break

print(sum(truncatables))
