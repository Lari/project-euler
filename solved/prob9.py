#!/usr/bin/python

from itertools import permutations
from math import sqrt

for c in range(1, 998):
    for b in range((1000 - c) // 2 + 1):
        a = 1000 - c - b
        if a**2 + b**2 == c**2:
            print(a*b*c)
