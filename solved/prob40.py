#!/usr/bin/python

d = "0"
i = 0
while True:
    i += 1
    d += str(i)
    if len(d) > 1000000:
        break
print(int(d[1]) * int(d[10]) * int(d[100]) * int(d[1000]) * int(d[10000]) *
        int(d[100000]) * int(d[1000000]))
