#!/usr/bin/pypy

from math import ceil

for magnitude in range(2, 8):
    lower = 10**magnitude
    upper = int(ceil(10**(magnitude + 1) / 6))
    for number in range(lower, upper):
        one = set(str(number))
        found = True
        for factor in range(2, 7): 
            if set(str(number * factor)) != one:
                found = False
                break
        if found == True:
            break
    if found == True:
        break
print(number)

