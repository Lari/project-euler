#!/usr/bin/pypy

from math import sqrt

def sum_of_proper_divisors(n):
    divisors = [1]
    root = sqrt(n)
    for i in range(2, int(root) + 1):
        if n % i == 0 and n != i:
            divisors.append(i)
            divisors.append(n / i)
    return sum(divisors)

summa = 0
for i in range(1, 10001):
    eka = sum_of_proper_divisors(i)
    toka = sum_of_proper_divisors(eka)
    if i == toka and i != eka:
        summa += eka + toka
print(summa // 2)

