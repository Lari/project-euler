#!/usr/bin/python

def divide(a, b):
    '''a/b in three parts: integer, non-recurring fractional part, and recurring part'''
    assert b>0
    integer = a // b
    r = a % b
    seen = {r: 0} #The position at which the remainder was first seen.
    digits = []
    while True :  #Loop is executed at most b times.
        r *= 10
        digits.append(r // b)
        r = r % b
        if r in seen:
            w = seen[r]
            return (integer, digits[:w], digits[w:])
        else:
            seen[r] = len(digits)

longest = {"value" : 0, "length" : 0}

for i in range(1, 1000):
    a, b, c = divide(1, i)
    if len(c) > longest["length"]:
        longest["length"] = len(c)
        longest["value"] = i
print(longest["value"])
