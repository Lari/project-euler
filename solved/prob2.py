#!/usr/bin/python

sum = 0
fib = 1
last = 1
while fib < 4e6:
    fib, last = last + fib, fib
    if fib % 2 == 0:
        sum += fib
print(sum)
