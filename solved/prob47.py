#!/usr/bin/pypy

# Find the first four consecutive integers to have four distinct primes factors.
# What is the first of these numbers?

from euler import factorize

last = [[], [], [], []] # last four numbers

i = 9
while True:
    i += 1
    last[i % 4] = set(factorize(i))
    if len(last[-1]) == len(last[-2]) == len(last[-3]) == len(last[-4]) == 4:
        if not last[-1] & last[-2] & last[-3] & last[-4]:
            print(i - 3)
            break
