#!/usr/bin/pypy

from euler import sieve

primes = sieve(1000000)
longest = 0
l_prime = 0

for i in range(len(primes)):
	for j in range(i, len(primes)):
		testable = sum(primes[i:j])
		if testable > 1000000:
			break
		if j - i > longest:
			if testable in primes:
				longest = j - i
				l_prime = testable
print l_prime
