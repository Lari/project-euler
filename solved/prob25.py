#!/usr/bin/python

def fibonacci():
    old = 0
    new = 1
    while True:
        yield new
        old, new = new, old + new

for i, v in enumerate(fibonacci()):
    if len(str(v)) == 1000:
        print(i + 1)
        break
