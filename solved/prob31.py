#!/usr/bin/python

def try_coins(left, coins):
    fitting = (i for i in coins if i <= left)
    combinations = 0
    for coin in fitting:
        if left - coin == 0:
            combinations += 1
        else:
            combinations += try_coins(left - coin, list((x for x in coins 
                                                         if x <= coin)))
    return combinations

coins = (1, 2, 5, 10, 20, 50, 100, 200)
print(try_coins(200, coins))
