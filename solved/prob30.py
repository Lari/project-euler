#!/usr/bin/python

total = 0
    
def sum_of_fifth_power_of_digits(n):
    if sum((int(n)**5 for n in str(n))) == n:
        print("found!:", n)
        return n
    else:
        return 0

for i in range(1, 1000000):
    total += sum_of_fifth_power_of_digits(i)

# remove 1 because even though 1 = 1^5, it is not a sum
print(total - 1)

