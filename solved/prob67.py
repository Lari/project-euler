#!/usr/bin/python

taulu = []

class memoized(object):
   """Decorator that caches a function's return value each time it is called.
   If called later with the same arguments, the cached value is returned, and
   not re-evaluated.
   """
   def __init__(self, func):
      self.func = func
      self.cache = {}
   def __call__(self, *args):
      try:
         return self.cache[args]
      except KeyError:
         value = self.func(*args)
         self.cache[args] = value
         return value
      except TypeError:
         # uncachable -- for instance, passing a list as an argument.
         # Better to not cache than to blow up entirely.
         return self.func(*args)
   def __repr__(self):
      """Return the function's docstring."""
      return self.func.__doc__
   def __get__(self, obj, objtype):
      """Support instance methods."""
      return functools.partial(self.__call__, obj)

def readTaulu():
    with open("triangle.txt") as f:
        while True:
            line = f.readline()
            if not line:
                break
            taulu.append([int(x) for x in line.split()])

@memoized
def larger(x, y):
    if y == len(taulu) - 1:
        return taulu[y][x]
    else:
        n1 = larger(x, y + 1)
        n2 = larger(x + 1, y + 1)
        if n1 > n2:
            return (n1 + taulu[y][x])
        else:
            return (n2 + taulu[y][x])

if __name__ == '__main__':
    readTaulu()
    print(larger(0, 0))
