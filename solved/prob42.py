#!/usr/bin/python

from string import ascii_uppercase

def triangle():
    n = 1
    while True:
        yield int((0.5 * n) * (n + 1))
        n += 1

with open("words.txt") as f:
    s = f.read()
words = [x[1:-1] for x in s.split(",")]

triangles = 0

for word in words:
    value = sum(ascii_uppercase.index(n) + 1 for n in word)
    for i in triangle():
        if value == i:
            triangles += 1
            break
        if i > value:
            break

print(triangles)
