#!/usr/bin/pypy

from math import sqrt
from itertools import combinations

def pentagonals():
    n = 1
    while True:
        yield int(n * (3 * n - 1) / 2)
        n += 1

def is_penta(y):
    if y <= 0:
        return False
    x = (sqrt(24 * y + 1) + 1) / 6
    return x % 1 == 0

def main():
#    for n in pentagonals():
#        pentag.append(n)
#        if len(pentag) == 10000000:
#            break

    pentag = []
    found = False
    for k in pentagonals():
        for i in pentag:
            if is_penta(k + i):
                if is_penta(k - i):
                    found = True
                    break
        if found:
            break
        pentag.append(k)
    print(k - i)

if __name__=='__main__':
    main()
